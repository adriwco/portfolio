## projeto-portfolio 
🔸Ainda modificarei muito! (estado de aprimoramento).
***
#### 🔸Tecnologias utilizadas:

[![HTML5](https://skills.thijs.gg/icons?i=html)](https://pt.wikipedia.org/wiki/HTML5)
[![CSS3](https://skills.thijs.gg/icons?i=css)](https://pt.wikipedia.org/wiki/CSS3)
[![JavaScript](https://skills.thijs.gg/icons?i=js)](https://pt.wikipedia.org/wiki/JavaScript)
[![Figma](https://skills.thijs.gg/icons?i=figma)](https://pt.wikipedia.org/wiki/Figma)
[![VSCode](https://skills.thijs.gg/icons?i=vscode)](https://pt.wikipedia.org/wiki/Visual_Studio_Code)
[![GitHub](https://skills.thijs.gg/icons?i=github)](https://pt.wikipedia.org/wiki/GitHub)

***
#### 🔸Pré-visualização (screenshot):
[![projeto-portfolio](https://user-images.githubusercontent.com/80191040/208557511-21e75c32-6763-4f87-a12e-88252c348145.png)](https://adriwco.github.io/projeto-portfolio)

***
### <p align="center">🔸[![License: MIT](https://img.shields.io/badge/License-MIT-red.svg)](https://opensource.org/licenses/MIT)🔸</p>
